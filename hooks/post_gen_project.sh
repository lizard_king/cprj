#!/bin/bash

if [[ {{cookiecutter.add_misc_directory}} == "yes" ]]; then
	mkdir misc
fi
if [[ {{cookiecutter.header_and_source_files_separately}} == "yes" ]]; then
	mkdir include
fi
if [[ {{cookiecutter.add_README}} == "no" ]]; then
	rm README.md
fi
if [[ {{cookiecutter.project_type}} == "cpp" ]]; then
	mv source/main.c source/main.cpp
fi

case "{{cookiecutter.license}}" in
	("MIT") 
        mv MIT_LICENSE LICENSE
	rm *_LICENSE
 	;;
	("Apache License 2.0")
	mv Apache_LICENSE LICENSE
        rm *_LICENSE
        ;;
    	("GNU GPLv3") 
	mv GPLv3_LICENSE LICENSE
	rm *_LICENSE
	;;
    	("Unlicense")
	mv Unlicense_LICENSE LICENSE
        rm *_LICENSE
        ;;
	("None")
	rm *_LICENSE
	;;
esac
